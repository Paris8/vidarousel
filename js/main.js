window.addEventListener("load", () => main());

const main = () => {
    // Video folder
    const videoFolder = "./videos";

    // TODO(enhancement): Use JSON instead of hardcoded objects

    // Video list, as a list of ["filename", "extension"]
    const videos = {
        1: ["nameOfFirstVideo", "mp4"],
        2: ["nameOfSecondVideo", "mp4"],
        3: ["nameOfThirdVideo", "mp4"],
        4: ["nameOfFourthVideo", "mp4"],
    };

    /* Add option for each videos
     * For example:
     * {
     *   1: [2],
     *   2: [3, 4]
     * };
     *
     * The video 1 offer the option to redirect to the video 2
     * The video 2 offer the option to redirect to the video 3 and 4 */
    const tree = {
        1: [2],
        2: [3, 4],
    };

    // Main Element who will receive the videos
    const mainElement = document.getElementsByTagName("main").item(0);

    // Current video showed to the user
    let currentVideo;

    // Add property to the player
    currentVideo = document.createElement("video");
    // TODO(enhancement): Custom controls?
    currentVideo.controls = true;
    // TODO(enhancement): Better fit window
    currentVideo.width = window.innerWidth - window.innerWidth / 2;
    currentVideo.style = "display: block; margin: auto;";

    // Add the video container to the document
    const source = document.createElement("source");
    currentVideo.appendChild(source);
    mainElement.appendChild(currentVideo);

    // Add the first video
    updateVideo(videoFolder, videos, tree, source, 1);

    // Add autoplay attributes for the next videos
    currentVideo.autoplay = true;
};

/**
 * Remove all buttons from an HTMLElement
 * @param {HTMLElement} parentOfButtons Parent element of the buttons
 */
const removeButtons = (parentOfButtons) => {
    const buttons = parentOfButtons.getElementsByTagName("button");
    console.log(buttons);
    while (buttons.length) {
        parentOfButtons.removeChild(buttons[0]);
    }
};

/**
 * Update the video and the buttons
 * @param {string} videoFolder Directory's name who contains videos
 * @param {{number: string[]}} videosList List of videos filenames
 * @param {{int: int[]}} tree List of all the videos from videosList with the next possibles choices from the video
 * @param {HTMLSourceElement} source Current video
 * @param {number} videoId Video ID to add to the main element
 */
const updateVideo = (videoFolder, videosList, tree, source, videoId) => {
    const videoElement = source.parentElement;
    const mainElement = videoElement.parentElement;
    source.src = `${videoFolder}/${videosList[videoId][0]}.${videosList[videoId][1]}`;
    source.type = `video/${videosList[videoId][1]}`;

    // Force the reload of the video
    videoElement.load();

    // Add buttons at the end of the video
    videoElement.addEventListener(
        "ended",
        () => {
            if (tree[videoId]) {
                tree[videoId].forEach((nextVideoId, counter) => {
                    const button = document.createElement("button");

                    // TODO(enhancement): Add custom label for the button redirection
                    const textButton = "Choix n°";

                    button.textContent = textButton + (counter + 1);
                    button.style =
                        "\
                font-size: 10vh; \
                margin-top: 5%; \
                margin-right: 5%;";

                    button.addEventListener("click", () => {
                        // Remove older buttons
                        removeButtons(mainElement);

                        // Update with the new video
                        updateVideo(
                            videoFolder,
                            videosList,
                            tree,
                            source,
                            nextVideoId
                        );
                    });

                    mainElement.appendChild(button);
                });
            } else {
                removeButtons(mainElement);
                const endText = document.createElement("h1");
                endText.style = "font-size: 11vh; color: white;";
                endText.innerText = "Merci d'avoir regardé !";
                mainElement.appendChild(endText);
            }
        },
        { once: true }
    );
};
